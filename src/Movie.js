
import React from 'react';
import MovieTags from './MovieTags';

class Movie extends React.Component {
  state = {
    name: 'The Boys',
    rated: '18+',
    description: 'Some superheroes in modern world',
    tags: ['heroes', 'cool', 'reality'],
    comment: '',
  }
  
  handleChange(e) {
    this.setState({
      comment: e.target.value
    })
  }

  render() {
    return (
      <div>
        <h2>Movie for today: {this.state.name}</h2>
        <h3>Tags: {<MovieTags tags={this.state.tags}/>}</h3>
        <h3>Rated: {this.state.rated}</h3>
        <h4>Comment: {this.state.comment}</h4>
        <input type="text" value={this.state.comment} onChange={this.handleChange.bind(this)} />
      </div>
    )
  }
}

export default Movie