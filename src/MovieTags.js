
import React from 'react';

class MovieTags extends React.Component {
  render() {
    const tags = this.props.tags.map(tag => {
      return <li>{tag}</li>
    })
    return (
      <ul>
        {tags}
      </ul>
    )
  }
}

export default MovieTags