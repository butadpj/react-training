import React from 'react';
import ReactDOM from 'react-dom';
import Movie from './Movie';
import './index.css';

const App = () => {
  return <Movie />
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

